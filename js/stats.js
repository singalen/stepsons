Game.Stats = function(node) {
	this._node = node;
}

Game.Stats.prototype.update = function(weapon, armor, hp, maxhp, radiationAbsorbed) {
	var parts = [];
	if (weapon) { parts.push("You are wielding %a.".format(weapon)); }
	if (armor) { parts.push("You are wearing %a.".format(armor)); }
	
	var className = (hp <= maxhp/3 ? "crit" : "");
	parts.push("Health: <span class='%s'>%s</span>/%s".format(className, hp, maxhp));

    className = (radiationAbsorbed > 2 ? "crit" : "");
    parts.push("Radiation absorbed: <span class='%s'>%s</span> mkSv".format(className, radiationAbsorbed));

	this._node.innerHTML = parts.join("<br/>");
}
