Game.Player.prototype._inventoryDialog = function() {
    Game.status.show("A–Z to use, ESC to exit");
    Game.Keyboard.forget("player"); // FIXME
    new Game.Dialog.Items("Inventory", this._items, this._inventoryDone.bind(this));
}

Game.Player.prototype._inventoryDone = function(item) {
    if (item) {
        this._tryUse(item);
    }
    Game.Keyboard.listen("player", this.handleEvent.bind(this));
}

Game.Player.prototype._tryUse = function(item) {
    var type = item.getType();

    if (Game.Items.is(type, "potion")) {
        this.removeItem(item);
        item.drink(this);
        return;
    }
    if (Game.Items.is(type, "food")) {
        this.removeItem(item);
        item.eat(this);
    }
}

Game.Player.prototype._equipmentDialog = function() {
    var equipment = [];
    if (this._weapon) { equipment.push(this._weapon); }
    if (this._armor) { equipment.push(this._armor); }

    if (!equipment) {
        Game.status.show("You're wearing nothing of in-game use!");
        return;
    }

    Game.status.show("A-B to swap, ESC to go back");
    Game.Keyboard.forget("player");

    new Game.Dialog.Items("Equipped", this._items, this._equipmentDone.bind(this));
}

Game.Player.prototype._equipmentDone = function(item) {
//    if (item) {
//        _tryUse(item);
//    }
    Game.Keyboard.listen("player", this.handleEvent.bind(this));
}

Game.Player.prototype._dropDialog = function() {
    Game.status.show("A–Z to drop, ESC to rethink");
    Game.Keyboard.forget("player");
    new Game.Dialog.Items("Drop from inventory", this._items, this._dropDone.bind(this));
}

Game.Player.prototype._dropDone = function(item) {
    if (item) {
        this.removeItem(item);
	    // FIXME: Push item out if there's already something here.
        this._level.setItem(item, this._position[0], this._position[1]);
	    Game.status.show("You drop %a.", item);
    }
    Game.Keyboard.listen("player", this.handleEvent.bind(this));
}
