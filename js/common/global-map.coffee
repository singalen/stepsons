# FIXME: for testing. How do I do this?
#if (!defined(window.Game))
window.Game = {}
Game = window.Game

#alert("Coffee!");
console.log "coffee"

Game.GlobalMap = (mapText) ->
	this.cells = {}
	br = mapText.search(/\r?\n\r?\n/)
	throw new Error("No section separator") if (br == -1)

	this.map = mapText.substring(0, br).split(/\r?\n/)
	this.fromTemplate(map)
	this.def = JSON.parse(mapText.substring(br));

Game.GlobalMap.prototype.getCell = (x, y) ->
	this.cells[x + "," + y]

Game.GlobalMap.prototype.setCell = (cell, x, y) ->
	this.cells[x + "," + y] = cell

Game.GlobalMap.prototype._fromTemplate = (map) ->
	width = Math.max line.length for line in map
	height = 0 #(for line in map when line).length
	for line of map
		for ch, x of line
			this.setCell(ch, x, height)
		height++ if (line)

Game.GlobalMap.prototype._levelFromPosition = (x, y) ->
	# like this:
	name = "level" + x + "," + y;
	if (name in this._cache)
		new Promise().fulfill(this._cache[name]);
	else
		this._levelFromTemplate(this._getGlobalCellTemplate(x, y));
