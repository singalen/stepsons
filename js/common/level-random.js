/**
 * Er, constructor
 * @param level Level object
 * @param defDsl Cell definitions from level.txt DSL with cell templates (and their probabilities).
 * @param templateCells A map of template chars to the cells' coordinates
 * @constructor
 */
Game.Level.RandomPlacer = function(level, defDsl, templateCells) {
	this._defDsl = defDsl;
	this._level = level;
	this._templateCells = templateCells;
	if (!templateCells) { throw new Error("templateCells is empty!"); }
};

Game.Level.RandomPlacer.prototype.getCount = function (countDsl) {
	if (typeof(countDsl) == "number") { return countDsl; }
	if (typeof(countDsl) == "object" && "min" in countDsl && "max" in countDsl) {
//			if ("avg" in countDsl) {
//				// TODO
//				var n = (countDsl.max - countDsl.min + 1);
//				var k1 = -(n*countDsl.avg-2*countDsl.min-n)/n;
//				var k2 = 2*(n*countDsl.avg-countDsl.min)/(n*(n-1));
//				if (ROT.RNG.getUniform() < k1) { return countDsl.min; }
//				return Math.floor(ROT.RNG.getUniform() * (countDsl.max - countDsl.min + 2)) + countDsl.min + 1;
//			} else {
		return Math.floor(ROT.RNG.getUniform() * (countDsl.max - countDsl.min + 1)) + countDsl.min;
//			}
	}
	throw new Error("number or {min:N, max:M [, avg:K]} expected, got " + countDsl);
};

Game.Level.RandomPlacer.prototype.addChaos = function() {
	if (typeof(this._defDsl.generate) != "object") { return this; }

	if (typeof(this._defDsl.generate.beings) == "object") {
		this._placeRandom(this._defDsl.generate.beings, function(beingCode, x, y) {
			var being = Game.Beings.create(beingCode);
			this._level.setBeing(being, x, y);
		}.bind(this));
	}

	if (typeof(this._defDsl.generate.items) == "object") {
		this._placeRandom(this._defDsl.generate.items, function(itemCode, x, y) {
			var item = Game.Items.create(itemCode);
			this._level.setItem(item, x, y);
		}.bind(this));
	}

	for (var cellCode in this._defDsl.generate) {
	//for (var i=0; i< this._defDsl.generate.length; i++) {
		this._setRandomCells(cellCode, this._defDsl.generate[cellCode]);
	}
	return this;
};

Game.Level.RandomPlacer.prototype._placeRandom = function(beings, placeCell) {
	for (var beingCode in beings) {

		//TODO: Don't place things into obstacles.
		var count = this.getCount(beings[beingCode]);
		console.log("Placing %s of %s".format(count, beingCode));

		for (var i = 0; i < count; i++) {
			var x = 0;
			var y = 0;
			var tries = 0;
			do {
				x = Math.floor(ROT.RNG.getUniform() * this._level._width);
				y = Math.floor(ROT.RNG.getUniform() * this._level._height);
				var cell = this._level.getCell(x, y);
				if (tries++ > 100) { return false; }
				// TODO: Break on too many tries
			} while (cell && cell.blocksMovement());

			if (beingCode.length == 1) {
				if (beingCode in this._defDsl) {
					this._level._fromChar(x, y, beingCode, this._defDsl);
				}
			} else {
				placeCell(beingCode, x, y)
			}
		}
	}

	return true;
};

/**
 * Set the cells marked as random on the map (this._templateCells) to a generated stuff.
 * @param cellDsls A list of items/cells to spread over this._templateCells
 * @param cellCode
 * @private
 */
Game.Level.RandomPlacer.prototype._setRandomCells = function(cellCode, cellDsls) {
	for (var i=0; i< cellDsls.length; i++) {
		var randomCellDsl = cellDsls[i];
		if (randomCellDsl.count && randomCellDsl.count.percent) {
			if (randomCellDsl.count.percent >= 100) { throw new Error("Percent too big!"); }

			var percentCount = this._templateCells[cellCode].length * randomCellDsl.count.percent / 100;
			this._setRandomCellsCount(cellCode, percentCount, randomCellDsl);
		} else {
			var count = this.getCount(randomCellDsl.count);
			this._setRandomCellsCount(cellCode, count, randomCellDsl);
		}
	}
};

Game.Level.RandomPlacer.prototype._setRandomCellsCount = function (cellCode, count, defDsl) {
	for (var i = 0; i < count; i++) {
		if (this._templateCells[cellCode].length == 0) { return; }
		var index = Math.floor(ROT.RNG.getUniform() * this._templateCells[cellCode].length);
		var replacedCell = this._templateCells[cellCode][index];
		this._templateCells[cellCode].splice(index, 1);
		this._level._fromCellTemplate(
			replacedCell[0], replacedCell[1], defDsl);
	}
};
