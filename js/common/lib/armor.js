Game.Item.Armor = function(type) {
	Game.Item.call(this, type);
	this._description = null;
	this._speed = 100;
	this._pv = 1;
    this._radiationResistance = 0.02; // Part of the getRadiation stopped
}

Game.Item.Armor.extend(Game.Item);

Game.Item.Armor.prototype.fromTemplate = function(template) {
	Game.Item.prototype.fromTemplate.call(this, template);
	if ("description" in template) { this._description = template.description; }
	if ("speed" in template) { this._speed = template.speed; }
    if ("radiationResistance" in template) { this._radiationResistance = template.radiationResistance; }
	return this;
}

Game.Item.Armor.prototype.getDescription = function() {
	return this._description;
}

Game.Item.Armor.prototype.getSpeed = function() {
	return this._speed;
}

Game.Item.Armor.prototype.getPV = function() {
	return this._pv;
}

Game.Item.Armor.prototype.getRadiationResistance = function() {
    return this._radiationResistance;
}
