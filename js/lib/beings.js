Game.Beings.define("player", {
	extend: "humanoid",
	pv: 2,
	hp: 20,
	damage: 2,
	ctor: Game.Player,
	color: [200, 200, 200]
}, {
	weight: 0
});

Game.Beings.define("rat", {
	extend: "animal",
	speed: 105,
	name: "rat",
	description: ["a regular white rat", "a lab rat mutant, about 10kg weight and 50 cm long"],
	hp: 2,
	damage: 1,
	hostile: true,
	tasks: ["slowwander", "attack", "wander"],
	color: [160, 160, 160],
	"char": "r"
});

Game.Beings.define("roach", {
	extend: "animal",
	speed: 105,
	name: "roach",
	description: ["a roach", "a radroach", "a 30-cm roach mutant dwelling in a farther areas"],
	hp: 2,
	damage: 3,
	hostile: true,
	tasks: ["slowwander", "attack", "wander"],
	color: [90, 220, 90],
	"char": "r"
}, {
	level: 2
});


Game.Beings.define("wolf", {
	extend: "animal",
	speed: 105,
	name: "wolf",
	hp: 3,
	damage: 2,
	hostile: true,
	tasks: ["attack", "wander"],
	color: [160, 160, 160],
	"char": "w"
});

Game.Beings.define("reindeer", {
	extend: "animal",
	speed: 105,
	name: "reindeer",
	hp: 2,
	damage: 0,
	hostile: true,
	tasks: ["wander"],
	color: [160, 160, 160],
	"char": "v"
});

Game.Beings.define("crew member", {
	extend: "humanoid",
	name: "Crew member",
	color: [160, 100, 100],
	chats: [
		"Hi!",
		"Nice day, isn't it?",
		"Will you help me with those rats?"
	]
}, {
	weight: 0
});

Game.Beings.define("mute", {
	"char": "o",
	name: "mute",
	description: ["Aargh, an ugly mute!", "twisted mutant",
		"mutant of the third degree. He would definitely get a black genetic card... if we were still civilized."],
	extend: "humanoid",
	hp: 3,
	damage: 2,
	hostile: true,
	tasks: ["wait till seen", "attack", "wander"],
	color: [30, 30, 240]
});

Game.Beings.define("mute chieftain", {
	extend: "mute",
	name: "mute chieftain",
	description: ["Aargh, two-headed mute!", "Two-headed mutant", "Now that's an interesting mutation - how do two heads control a single body?"],
	hp: 4,
	speed: 105,
	pv: 2,
	damage: 3,
	"char": "M"
}, {
	level: 2
});

Game.Beings.define("drone", {
//	extend: "being",
	name: "drone",
	description: ["shiny buzzy!", "a small service robot", "AI worker drone"],
	hp: 2,
	damage: 0,
	speed: 100,
	"char": "d",
	color: [120, 120, 220]
}, {
	weight: 0
});

Game.Beings.define("soldier", {
	extend: "drone",
	name: "soldier",
	description: ["bigger shiny buzzy!", "a larger service robot", "AI scout armed with a welding laser"],
	hp: 12,
	pv: 2,
	damage: 5,
	speed: 105,
	"char": "d",
	color: [160, 160, 250]
}, {
	level: 2
});

Game.Beings.define("terminator", {
	extend: "drone",
	name: "terminator",
	description: ["Big shiny man!", "a very large humanoid robot with several arms",
		"A multi-purpose repair robot changed into a war machine!"],
	hp: 40,
	pv: 6,
	damage: 15,
	speed: 110,
	"char": "D",
	color: [160, 250, 250]
}, {
	level: 3
});

// http://en.wikipedia.org/wiki/Alien_(creature_in_Alien_franchise)#Name
Game.Beings.define("baby alien", {
	extend: "animal",
	speed: 120,
	name: "baby alien",
	description: ["a small worm monster", "a small alien", "larva stage of a Xenomorph"],
	hp: 30,
	damage: 10,
	hostile: true,
	tasks: ["attack", "wander"],
	color: [230, 230, 20],
	"char": "a"
});

Game.Beings.define("alien", {
	extend: "animal",
	speed: 130,
	name: "alien",
	description: ["man-monster", "an adult alien", "adult stage of a Xenomorph"],
	hp: 50,
	damage: 20,
	hostile: true,
	tasks: ["attack", "wander"],
	color: [230, 230, 20],
	"char": "A"
});
