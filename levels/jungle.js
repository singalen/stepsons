Game.Level.Jungle = function() {
	Game.Level.call(this);
	this._lighting.setOptions({range:6});
};
Game.Level.Jungle.extend(Game.Level);

Game.Level.Jungle.prototype.fromTemplate = function(map, def) {
	Game.Level.prototype.fromTemplate.call(this, map, def);

    var waterMaze = new ROT.Map.Cellular(this._width, this._height);
    waterMaze.randomize(0.3);
    waterMaze.create(); waterMaze.create(); waterMaze.create();
    waterMaze.create(function(x, y, type) {
        if (type == 1) {
            var cell = Game.Cells.create("water");
            this.setCell(cell, x, y);
        }
    }.bind(this));

	var maze = new ROT.Map.Cellular(this._width, this._height);
	maze.randomize(0.4);

	maze.create(function(x, y, type) {
		var cellType = (type == 1 ? "tree" : "ground");
		// TODO: Take map type, tree type, ground types from def
        if (!this.getCell(x, y)) {
            var cell = Game.Cells.create(cellType);
            this.setCell(cell, x, y);
        }
	}.bind(this));

	return this;
};