# Stepsons of the Universe

A roguelike game created (and failed) for 7drl challenge.

Mainly by chopping down and then trashing ondras' (https://github.com/ondras)
amazing rot.js (http://ondras.github.com/rot.js/hp/).

## Intro

You're in a primitive tribe living in a caves near a jungle.
Or, at least, this is the way it looks from the beginning.

## Controls

Use cursor/keypad keys to move, keypad "5" to wait, and:
* I: Inventory
* G,: pick up
* D: drop
* E: Equip/unequip things